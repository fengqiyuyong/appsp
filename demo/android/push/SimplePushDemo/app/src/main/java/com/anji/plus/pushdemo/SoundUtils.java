package com.anji.plus.pushdemo;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * Appsp-Push 播放声音
 * </p>
 */
public class SoundUtils {
    public static SoundPool mSoundPlayer = new SoundPool(10,
            AudioManager.STREAM_SYSTEM, 5);
    public static SoundUtils soundPlayUtils;
    static Context mContext;

    /**
     * 初始化
     * @param context
     */
    public static SoundUtils init(Context context) {
        if (soundPlayUtils == null) {
            soundPlayUtils = new SoundUtils();
        }
        // 初始化声音
        mContext = context;
        mSoundPlayer.load(mContext, R.raw.hua_notice, 1);// 1
        mSoundPlayer.load(mContext, R.raw.error, 1);// 2
        return soundPlayUtils;
    }

    //播放声音
    public static void play(int soundID) {
        mSoundPlayer.play(soundID, 1, 1, 0, 0, 1);
    }
}
