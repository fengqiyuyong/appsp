package com.anji.sp.push.model.vo;

import java.io.Serializable;

/**
 * 华为推送回调Bean
 */
public class HuaweiCallBackBean implements Serializable {
    private static final long serialVersionUID = 1L;

    private String biTag;
    private String appid;
    private String token;
    private String status;//0成功
    // 参考：https://developer.huawei.com/consumer/cn/doc/development/HMSCore-Guides-V5/msg-receipt-guide-0000001050040176-V5#ZH-CN_TOPIC_0000001050040176__p121151147184318
    private String timestamp;
    private String requestId;

    public String getBiTag() {
        return biTag;
    }

    public void setBiTag(String biTag) {
        this.biTag = biTag;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
/**
 * {
 *     "statuses": [
 *         {
 *             "biTag": "131415",
 *             "appid": "0000000013",
 *             "token": "14889646529623230000000013000001",
 *             "status": 0,
 *             "timestamp": 1489993883189,
 *             "requestId": "1234567890"
 *         },
 *         {
 *             "biTag": "131415",
 *             "appid": "0000000013",
 *             "token": "14889646529623230000000113000001",
 *             "status": 2,
 *             "timestamp": 1489993888189,
 *             "requestId": "1234567890"
 *         }
 *     ]
 * }
 */
}
