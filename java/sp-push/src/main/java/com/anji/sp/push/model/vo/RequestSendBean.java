package com.anji.sp.push.model.vo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * 发送消息的参数实体类
 */
public class RequestSendBean {
    private ArrayList<String> deviceIds;//设备的唯一id
    private List<String> tokens;//厂商通道的tokens 通过deviceIds查询
    private List<String> registrationIds;//极光registrationIds 通过deviceIds查询
    private String title;//推送标题
    private String content;//推送内容
    private Map<String, String> extras;//传递的参数
    private Map<String, Object> androidConfig;//iOS配置项 json {"sound": "sound"}
    private Map<String, Object> iosConfig;//android配置项 json {"sound": "sound"}
    private String pushType;//1：透传 走极光 0：走厂商通道 推送类型 1透传消息 0 普通消息(推送通知 默认)
    private String appKey;
    private String secretKey;
    private String sendOrigin = "1";
    private String deviceType;//设备类型 通过uuids查询

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public Map<String, String> getExtras() {
        return extras;
    }

    public void setExtras(Map<String, String> extras) {
        this.extras = extras;
    }

    public Map<String, Object> getAndroidConfig() {
        return androidConfig;
    }

    public void setAndroidConfig(Map<String, Object> androidConfig) {
        this.androidConfig = androidConfig;
    }

    public Map<String, Object> getIosConfig() {
        return iosConfig;
    }

    public void setIosConfig(Map<String, Object> iosConfig) {
        this.iosConfig = iosConfig;
    }

    public String getSendOrigin() {
        return sendOrigin;
    }

    public void setSendOrigin(String sendOrigin) {
        this.sendOrigin = sendOrigin;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }

    public List<String> getRegistrationIds() {
        if (null != registrationIds && registrationIds.size() > 0) {
            registrationIds.removeAll(Collections.singleton(""));
        }
        return registrationIds;
    }

    public void setRegistrationIds(List<String> registrationIds) {
        if (null != registrationIds && registrationIds.size() > 0) {
            registrationIds.removeAll(Collections.singleton(""));
        }
        this.registrationIds = registrationIds;
    }

    public ArrayList<String> getDeviceIds() {
        return deviceIds;
    }

    public void setDeviceIds(ArrayList<String> deviceIds) {
        this.deviceIds = deviceIds;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPushType() {
        return pushType;
    }

    public void setPushType(String pushType) {
        this.pushType = pushType;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    @Override
    public String toString() {
        return "RequestSendBean{" +
                "deviceIds=" + deviceIds +
                ", tokens=" + tokens +
                ", registrationIds=" + registrationIds +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", extras=" + extras +
                ", androidConfig=" + androidConfig +
                ", iosConfig=" + iosConfig +
                ", pushType='" + pushType + '\'' +
                ", appKey='" + appKey + '\'' +
                ", sendOrigin='" + sendOrigin + '\'' +
                ", deviceType='" + deviceType + '\'' +
                '}';
    }
}
