package com.anji.sp.push.mapper;

import com.anji.sp.push.model.po.PushMessagePO;
import com.anji.sp.push.model.vo.PushMessageVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 服务推送id记录 Mapper 接口
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
public interface PushMessageMapper extends BaseMapper<PushMessagePO> {

    /** XML 自定义分页
     * @param page
     * @param pushMessageVO
     * @return
     */
    IPage<PushMessageVO> queryByPage(Page<?> page, @Param("pushMessageVO") PushMessageVO pushMessageVO);
}
