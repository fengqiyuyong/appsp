package com.anji.plus.ajpushlibrary.http;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * 接口地址
 * </p>
 */

public class AppSpPushRequestUrl {
    //默认基础地址，可改变
    public static String Host = "http://uatappsp.anji-plus.com";//uat环境
    public static final String putPushInfo = "/sp/push/init";
}
